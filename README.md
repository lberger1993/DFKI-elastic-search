## Project Setup:

Elastic Search for ROR DFKI project


## to check the data
start elastic search locally

`http://localhost:9200/rsc-dbt/_search?pretty=true&q=*:*`

Known indexes:
rsc-dbt


## to upload to new indexes with correct csv file
`cd bulk-upload && upload.py {FILENAME.csv} {index-name}`


## helpful curl requests



`curl -XGET 'localhost:9200/resources/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "query": {
    "filtered": {
            "query": {
                "match": {"location_id":123}
                }
            },
            "filter": {
            "range" : {
            "date" : {
                "gte": "01/01/2017",
                "lte": "31/01/2017",
                "format": "dd/MM/yyyy||dd/MM/yyyy"
            }
           }
        }
    }
}
'`

Delete an index:

`curl -XDELETE 'localhost:9200/{INDEX}?pretty'`

Create an index (preferred is with bulk upload)

`curl -XPUT 'localhost:9200/{INDEX}?pretty' -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3,
            "number_of_replicas" : 2
        }
    }
}
'`
