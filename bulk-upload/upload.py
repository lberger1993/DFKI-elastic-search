import csv
import sys

from elasticsearch import helpers, Elasticsearch

es = Elasticsearch()

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        reader = csv.DictReader(f)
        helpers.bulk(es, reader, index=str(sys.argv[2]), doc_type='time-series')